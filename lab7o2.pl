#!/usr/bin/perl
use strict;
use warnings;
my @Beatles = qw(John Paul George Ringo);
print "@Beatles\n";
my ($drummer, @vocals);
push @vocals, shift @Beatles;
$drummer = pop @Beatles;
push @vocals, shift @Beatles;
push @vocals, shift @Beatles;
push @Beatles, 'Pete Best';
shift @Beatles;
push @Beatles, $drummer;
unshift @Beatles, @vocals;
print "@Beatles\n";